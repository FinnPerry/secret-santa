"""
script for generating a list of who buys for who in a secret santa
"""

import argparse
import shlex
import os
import random


class Person:
    """
    class for storing data about a person
    """

    def __init__(self, first_name="", last_name="", is_taken=False):
        self.first_name = first_name
        self.last_name = last_name
        self.is_taken = is_taken

    def __str__(self):
        return (
            '"' + self.first_name + '" "' + self.last_name + '" ' + str(self.is_taken)
        )

    def __repr__(self):
        return self.__str__()

    def full_name(self):
        """
        get the full name
        """
        return self.first_name + " " + self.last_name


def parse_args():
    """
    parge cli args
    """

    parser = argparse.ArgumentParser(
        description="Generate a list of who buys for who in a secret santa."
    )
    parser.add_argument("input_file", type=str)
    parser.add_argument(
        "output_file", type=str, nargs="?", default="secret-santa-output.txt"
    )
    return parser.parse_args()


def read_people(file_path):
    """
    read a list of names from a file
    """

    # check if the file exists
    if not os.path.exists(file_path):
        return None

    # read input file
    lines = []
    with open(file_path, "rt", encoding="utf-8") as file:
        lines = file.readlines()
    for i, j in enumerate(lines):
        lines[i] = j.replace("\n", "")

    # get list of people
    people = []
    for i in lines:
        split = shlex.split(i)
        if len(split) == 0:
            continue
        person = Person(first_name=split[0])
        if len(split) > 1:
            person.last_name = " ".join(split[1:])
        people.append(person)
    return people


def get_elegible(people, i):
    """
    get a list of people elegible to pair with the current person
    """

    candidates = []
    for j in people:
        # skip self
        if i is j:
            continue
        # skip taken people
        if j.is_taken:
            continue
        # skip people from same family
        if i.last_name == j.last_name:
            continue
        candidates.append(j)
    return candidates


def get_partner(people, i):
    """
    get a random partner for a person
    """

    # get elegible list
    elegible = get_elegible(people, i)
    if len(elegible) == 0:
        return None
    # pick a random elegible person
    partner = elegible[random.randint(0, len(elegible) - 1)]
    return partner


def map_partners(people):
    """
    generate a map of who buys for who
    """

    # create a map of assigned partners
    pairs = {}
    for i in people:
        partner = get_partner(people, i)
        pairs[i] = partner
        if partner is not None:
            partner.is_taken = True
    return pairs


def output_partner_map(file_path, pairs):
    """
    output the partner map to stdout and a file
    """
    # create strings
    lines = []
    for i in pairs:
        line = ""
        if pairs[i] is None:
            line = "Error: No elegible partners found for " + i.full_name()
        else:
            line = i.full_name() + " buys for " + pairs[i].full_name()
        lines.append(line + "\n")
        print(line)
    # write to file
    with open(file_path, "wt", encoding="utf-8") as file:
        file.writelines(lines)
    print("Wrote output to", file_path)


def main():
    """
    main function
    """

    # parge args
    pargs = parse_args()

    # get people list
    people = read_people(pargs.input_file)
    if people is None:
        print("Error: Input file does not exist.")
        return

    # pick random pairs
    pairs = map_partners(people)

    # write to output file
    output_partner_map(pargs.output_file, pairs)


if __name__ == "__main__":
    main()

# Secret Santa Pairs Generator

## How to use:

1. Download the program and open a terminal in the program folder.
2. Install Python3 with this command:
    - `sudo apt update && sudo apt install -y python3`.
3. Create a file called "names.txt" and fill it in with names.
    - Example file contents:
        ```
        Kai Davies
        Sally Chung
        Ammar Chapman
        ```
4. Run the program with: `./run`.
    - This will generate a file called "secret-santa-output.txt" containing
        a list of who buys for who.
    - People with the same last name will not be picked to buy for each other.
    - Depending on your list of names, it may be impossible for the program to
        find a valid pairing for everyone. In this case it will fill in as much
        as it can.
